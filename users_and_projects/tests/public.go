package tests

import (
	"fmt"
	"gitlab.com/slon/shad-ds/users_and_projects/interfaces"
	"gitlab.com/slon/shad-ds/users_and_projects/lib"
)

func sliceContains(value string, slice []string) bool {
	for _, element := range slice {
		if element == value {
			return true
		}
	}
	return false
}

func FixedDelay(message lib.SentMessage) int {
	return message.SendTimestamp + 5
}

func OneWayDelay(message lib.SentMessage) int {
	if message.Receiver == interfaces.UsersActorId {
		return lib.MaxTimestamp + 1
	} else {
		return message.SendTimestamp + 1
	}
}

func SimpleClient(actorSystem *lib.ActorSystem) error {
	client := lib.NewClient(actorSystem)

	if err := <-client.CreateUser("psushin", []string{}); err != nil {
		return err
	}

	if err := <-client.CreateProject("YT", []string{"psushin"}); err != nil {
		return err
	}

	getResult := <-client.GetUserProjects("psushin", true)
	if getResult.Err != nil {
		return getResult.Err
	}

	if !sliceContains("YT", getResult.Values) {
		return fmt.Errorf("cannot find project \"YT\" for user \"psushin\" after successful write")
	}

	return nil
}

var ExampleTests = []lib.TestDescriptor{
	{
		TestName:     "One client, fixed delay",
		Clients:      []lib.ClientFunc{SimpleClient},
		DelayMessage: FixedDelay,
	},
}

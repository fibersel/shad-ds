package interfaces

import (
	"google.golang.org/protobuf/proto"
)

const MaxMessageSize = 100 * 1024

type Message struct {
	Payload proto.Message
	Sender  string
}

type Actor interface {
	Receive(message Message)
}

type ActorContext interface {
	Send(payload proto.Message, receiver string) error
}

const ProjectsActorId = "projects"

type ProjectsServiceCtor func(context ActorContext) Actor

const UsersActorId = "users"

type UsersServiceCtor func(context ActorContext) Actor

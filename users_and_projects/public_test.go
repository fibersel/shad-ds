package fifo

import (
	"gitlab.com/slon/shad-ds/users_and_projects/lib"
	"gitlab.com/slon/shad-ds/users_and_projects/solution"
	"gitlab.com/slon/shad-ds/users_and_projects/tests"
	"testing"
)

func TestPublic(t *testing.T) {
	lib.RunTests(t, solution.NewUsersService, solution.NewProjectsService, tests.ExampleTests)
}

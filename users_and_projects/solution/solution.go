package solution

import (
	"fmt"
	"gitlab.com/slon/shad-ds/users_and_projects/interfaces"
)

/////////////////////////////////////////////////////////////////////////////////////////////

type ProjectsService struct {
	context interfaces.ActorContext
}

func (s *ProjectsService) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateProjectReq:
		fmt.Printf(
			"Received create project request [project: %v, users: %v, sender: %v, request_id: %v]\n",
			m.Project,
			m.Users,
			message.Sender,
			m.RequestId)

		var rsp interfaces.CreateProjectRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{Ok: true}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			fmt.Printf(
				"Failed to send create project response [request_id :%v, receiver: %v, error: %v]\n",
				rsp.RequestId,
				message.Sender,
				err)
		}

	case *interfaces.GetProjectUsersReq:
		fmt.Printf(
			"Received get user projects request [project: %v, consistent: %v, sender: %v, request_id :%q]\n",
			m.Project,
			m.Consistent,
			message.Sender,
			m.RequestId)

		var rsp interfaces.GetProjectUsersRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{
			Ok:      false,
			Message: "Service is unavailable",
		}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			fmt.Printf(
				"Failed to send get project users response [request_id :%v, receiver: %v, error: %v]\n",
				rsp.RequestId,
				message.Sender,
				err)
		}

	default:
		fmt.Printf("Ignore unexpected message type for project service [message_type: %T, sender: %v]\n", message.Payload, message.Sender)
	}
}

func NewProjectsService(context interfaces.ActorContext) interfaces.Actor {
	return &ProjectsService{
		context: context,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

type UsersService struct {
	context interfaces.ActorContext
}

func (s *UsersService) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateUserReq:
		fmt.Printf(
			"Received create user request [user: %v, projects: %v, sender: %v, request_id :%q]\n",
			m.User,
			m.Projects,
			message.Sender,
			m.RequestId)

		var rsp interfaces.CreateUserRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{Ok: true}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			fmt.Printf(
				"Failed to send create user response [request_id :%v, receiver: %v, error: %v]\n",
				rsp.RequestId,
				message.Sender,
				err)
		}

	case *interfaces.GetUserProjectsReq:
		fmt.Printf(
			"Received get user projects request [user: %v, consistent: %v, sender: %v, request_id :%q]\n",
			m.User,
			m.Consistent,
			message.Sender,
			m.RequestId)

		var rsp interfaces.GetUserProjectsRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{
			Ok:      false,
			Message: "Service is unavailable",
		}

		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			fmt.Printf(
				"Failed to send get user projects response [request_id :%v, receiver: %v, error: %v]\n",
				rsp.RequestId,
				message.Sender,
				err)
		}

	default:
		fmt.Printf("Ignore unexpected message type for users service [message_type: %T, sender: %v]\n", message.Payload, message.Sender)
	}
}

func NewUsersService(context interfaces.ActorContext) interfaces.Actor {
	return &UsersService{
		context: context,
	}
}

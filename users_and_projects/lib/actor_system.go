package lib

import (
	"container/heap"
	"fmt"
	"gitlab.com/slon/shad-ds/users_and_projects/interfaces"
	"google.golang.org/protobuf/proto"
)

type ActorSystem struct {
	delayMessage   MessageDelayFunc
	actors         map[string]interfaces.Actor
	clients        map[string]*Client
	sentMessages   chan SentMessage
	queuedMessages MessageQueue
	timestamp      int
}

func (as *ActorSystem) registerActor(id string, actor interfaces.Actor) (err error) {
	if _, ok := as.actors[id]; ok {
		return fmt.Errorf("actor %q already exists", id)
	}

	as.actors[id] = actor
	return err
}

func (as *ActorSystem) registerClient(id string, client *Client) (err error) {
	if _, ok := as.clients[id]; ok {
		return fmt.Errorf("client %q already exists", id)
	}

	as.registerActor(id, client)

	as.clients[id] = client
	return err
}

func (as *ActorSystem) enqueueSentMessage(sentMessage SentMessage) {
	sentMessage.SendTimestamp = as.timestamp
	sentMessage.DeliverTimestamp = as.timestamp + 1

	_, senderIsClient := as.clients[sentMessage.Sender]
	_, receiverIsClient := as.clients[sentMessage.Receiver]

	if !senderIsClient && !receiverIsClient {
		sentMessage.DeliverTimestamp = as.delayMessage(sentMessage)
	}

	heap.Push(&as.queuedMessages, sentMessage)
}

func (as *ActorSystem) deliverNextMessage() bool {
	if len(as.queuedMessages) == 0 {
		return false
	}

	sentMessage := heap.Pop(&as.queuedMessages).(SentMessage)

	as.timestamp = sentMessage.DeliverTimestamp

	payload := proto.Clone(sentMessage.PayloadThunk)
	proto.Unmarshal(sentMessage.Payload, payload)

	message := interfaces.Message{
		Payload: payload,
		Sender:  sentMessage.Sender}

	as.actors[sentMessage.Receiver].Receive(message)
	return true
}

func (as *ActorSystem) getCurrentTimestamp() int {
	return as.timestamp
}

func (as *ActorSystem) shutdown() {
	for _, client := range as.clients {
		client.Shutdown()
	}
}

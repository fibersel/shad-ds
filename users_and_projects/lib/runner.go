package lib

import (
	"fmt"
	"gitlab.com/slon/shad-ds/users_and_projects/interfaces"
	"runtime/debug"
	"testing"
	"time"
)

func RunTest(
	test TestDescriptor,
	usersCtor interfaces.UsersServiceCtor,
	projectsCtor interfaces.ProjectsServiceCtor) (err error) {

	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v\nStack:\n%s\n", e, debug.Stack())
		}
	}()

	as := &ActorSystem{
		delayMessage:   test.DelayMessage,
		actors:         make(map[string]interfaces.Actor),
		clients:        make(map[string]*Client),
		queuedMessages: []SentMessage{},
		sentMessages:   make(chan SentMessage, 100),
		timestamp:      0,
	}

	usersActorContext := ActorContext{
		actorSystem: as,
		id:          interfaces.UsersActorId,
	}
	usersActor := usersCtor(&usersActorContext)
	err = as.registerActor(interfaces.UsersActorId, usersActor)
	if err != nil {
		panic("failed to register \"users\" service actor")
	}

	projectsActorContext := ActorContext{
		actorSystem: as,
		id:          interfaces.ProjectsActorId,
	}
	projectsActor := projectsCtor(&projectsActorContext)
	err = as.registerActor(interfaces.ProjectsActorId, projectsActor)
	if err != nil {
		panic("failed to register \"projects\" service actor")
	}

	clientResults := make(chan error, len(test.Clients))

	for _, client := range test.Clients {
		c := client
		go func() {
			clientResults <- c(as)
		}()
	}

	finishedClients := 0

	finishSimulation := func() bool {
		return as.getCurrentTimestamp() > MaxTimestamp ||
			(finishedClients == len(test.Clients) && len(as.queuedMessages) == 0)
	}

	for !finishSimulation() {
		select {
		case sentMessage := <-as.sentMessages:
			as.enqueueSentMessage(sentMessage)

		case clientResult := <-clientResults:
			if clientResult != nil {
				return clientResult
			}
			finishedClients += 1

		default:
			if !as.deliverNextMessage() {
				time.Sleep(time.Millisecond)
			}
		}
	}

	as.shutdown()

	for i := 0; i < len(test.Clients)-finishedClients; i++ {
		err = <-clientResults
		if err != nil {
			return
		}
	}
	return err
}

func RunTests(
	t *testing.T,
	usersCtor interfaces.UsersServiceCtor,
	projectsCtor interfaces.ProjectsServiceCtor,
	tests []TestDescriptor) {
	for _, test := range tests {
		fmt.Printf("# Running test %q\n", test.TestName)
		t.Run(test.TestName, func(t *testing.T) {
			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, usersCtor, projectsCtor)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					t.Errorf("Test failed: %v", err)
				}

			case <-time.After(TestTimeout):
				t.Errorf("Test timed out")
			}
		})
	}
}

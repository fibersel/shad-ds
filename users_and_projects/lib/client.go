package lib

import (
	"fmt"
	"gitlab.com/slon/shad-ds/users_and_projects/interfaces"
)

var MonotonicRequestId = 0
var MonotonicClientId = 0

func generateId() string {
	MonotonicRequestId += 1
	return fmt.Sprintf("request_%v", MonotonicRequestId)
}

func validateSender(expectedSender string, message interfaces.Message) {
	if expectedSender != message.Sender {
		panic(fmt.Errorf("expected sender %q, but got %q", expectedSender, message.Sender))
	}
}

type GetResponse struct {
	Err    error
	Values []string
}

type CreateResponseChannel chan error
type GetResponseChannel chan GetResponse

type Client struct {
	Id                   string
	OutstandingRequestId *string
	OutstandingCreate    CreateResponseChannel
	OutstandingGet       GetResponseChannel
	ActorSystem          *ActorSystem
	ActorContext         *ActorContext
}

func (c *Client) Shutdown() {
	if c.OutstandingCreate != nil {
		responseChannel := c.OutstandingCreate
		c.OutstandingCreate = nil
		responseChannel <- fmt.Errorf("simulation finished")
	}

	if c.OutstandingGet != nil {
		responseChannel := c.OutstandingGet
		c.OutstandingGet = nil
		getResponse := GetResponse{
			Err:    fmt.Errorf("simulation finished"),
			Values: nil,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateUserRsp:
		validateSender(interfaces.UsersActorId, message)
		c.processCreateUserResponse(m)

	case *interfaces.CreateProjectRsp:
		validateSender(interfaces.ProjectsActorId, message)
		c.processCreateProjectResponse(m)

	case *interfaces.GetUserProjectsRsp:
		validateSender(interfaces.UsersActorId, message)
		c.processGetUserProjectsResponse(m)

	case *interfaces.GetProjectUsersRsp:
		validateSender(interfaces.ProjectsActorId, message)
		c.processGetProjectUsersResponse(m)

	default:
		panic(fmt.Errorf("client received unexpected message type %T", message))
	}
}

func (c *Client) processCreateUserResponse(message *interfaces.CreateUserRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingCreate == nil {
		panic("received create user response, but response channel does not exist")
	}
	responseChannel := c.OutstandingCreate
	c.OutstandingCreate = nil
	if message.Error.Ok {
		responseChannel <- nil
	} else {
		responseChannel <- fmt.Errorf(message.Error.Message)
	}
}

func (c *Client) processGetUserProjectsResponse(message *interfaces.GetUserProjectsRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingGet == nil {
		panic("received get user projects response, but response channel does not exist")
	}
	responseChannel := c.OutstandingGet
	c.OutstandingGet = nil
	if message.Error.Ok {
		getResponse := GetResponse{
			Err:    nil,
			Values: message.Projects,
		}
		responseChannel <- getResponse
	} else {
		getResponse := GetResponse{
			Err:    fmt.Errorf(message.Error.Message),
			Values: message.Projects,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) processCreateProjectResponse(message *interfaces.CreateProjectRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingCreate == nil {
		panic("received create project response, but response channel does not exist")
	}
	responseChannel := c.OutstandingCreate
	c.OutstandingCreate = nil
	if message.Error.Ok {
		responseChannel <- nil
	} else {
		responseChannel <- fmt.Errorf(message.Error.Message)
	}
}

func (c *Client) processGetProjectUsersResponse(message *interfaces.GetProjectUsersRsp) {
	c.validateResponseId(message.RequestId)
	if c.OutstandingGet == nil {
		panic("received get user projects response, but response channel does not exist")
	}
	responseChannel := c.OutstandingGet
	c.OutstandingGet = nil
	if message.Error.Ok {
		getResponse := GetResponse{
			Err:    nil,
			Values: message.Users,
		}
		responseChannel <- getResponse
	} else {
		getResponse := GetResponse{
			Err:    fmt.Errorf(message.Error.Message),
			Values: message.Users,
		}
		responseChannel <- getResponse
	}
}

func (c *Client) validateConcurrentRequests() {
	if c.OutstandingRequestId != nil {
		panic("client cannot serve concurrent requests")
	}
}

func (c *Client) validateResponseId(requestId string) {
	if c.OutstandingRequestId == nil {
		panic("client was not expecting response")
	}

	if *c.OutstandingRequestId != requestId {
		panic(fmt.Errorf("received response with unexpected id %q instead of %q", requestId, c.OutstandingRequestId))
	}

	c.OutstandingRequestId = nil
}

func (c *Client) CreateUser(user string, projects []string) CreateResponseChannel {
	c.validateConcurrentRequests()

	requestId := generateId()
	responseChan := make(CreateResponseChannel)

	c.OutstandingRequestId = &requestId
	c.OutstandingCreate = responseChan

	var req interfaces.CreateUserReq
	req.RequestId = requestId
	req.User = user
	req.Projects = projects

	err := c.ActorContext.Send(&req, interfaces.UsersActorId)
	if err != nil {
		panic(fmt.Errorf("failed to send message to \"users\" actor, %v", err))
	}
	return responseChan
}

func (c *Client) CreateProject(project string, users []string) CreateResponseChannel {
	c.validateConcurrentRequests()

	requestId := generateId()
	responseChan := make(CreateResponseChannel)

	c.OutstandingRequestId = &requestId
	c.OutstandingCreate = responseChan

	var req interfaces.CreateProjectReq
	req.RequestId = requestId
	req.Project = project
	req.Users = users

	err := c.ActorContext.Send(&req, interfaces.ProjectsActorId)
	if err != nil {
		panic(fmt.Errorf("failed to send message to \"projects\" actor, %v", err))
	}
	return responseChan
}

func (c *Client) GetUserProjects(user string, consistent bool) GetResponseChannel {
	c.validateConcurrentRequests()

	requestId := generateId()
	responseChan := make(GetResponseChannel)

	c.OutstandingRequestId = &requestId
	c.OutstandingGet = responseChan

	var req interfaces.GetUserProjectsReq
	req.RequestId = requestId
	req.User = user
	req.Consistent = consistent

	err := c.ActorContext.Send(&req, interfaces.UsersActorId)
	if err != nil {
		panic(fmt.Errorf("failed to send message to \"users\" actor, %v", err))
	}
	return responseChan
}

func (c *Client) GetProjectUsers(project string, consistent bool) GetResponseChannel {
	c.validateConcurrentRequests()

	requestId := generateId()
	responseChan := make(GetResponseChannel)

	c.OutstandingRequestId = &requestId
	c.OutstandingGet = responseChan

	var req interfaces.GetProjectUsersReq
	req.RequestId = requestId
	req.Project = project
	req.Consistent = consistent

	err := c.ActorContext.Send(&req, interfaces.ProjectsActorId)
	if err != nil {
		panic(fmt.Errorf("failed to send message to \"projects\" actor, %v", err))
	}
	return responseChan
}

func NewClient(actorSystem *ActorSystem) *Client {
	MonotonicClientId += 1
	clientId := fmt.Sprintf("client_%v", MonotonicClientId)
	context := ActorContext{
		actorSystem: actorSystem,
		id:          clientId,
	}
	client := Client{
		Id:           clientId,
		ActorContext: &context,
		ActorSystem:  actorSystem,
	}
	err := actorSystem.registerClient(clientId, &client)
	if err != nil {
		panic(fmt.Errorf("failed to register new client, %v", err))
	}
	return &client
}

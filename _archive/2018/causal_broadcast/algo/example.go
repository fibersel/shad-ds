package algo

import (
	"container/heap"
	"fmt"
	"gitlab.com/slon/shad-ds/causal_broadcast/lib"
)

// Types of messages sent over the "wire".
type BroadcastMessage struct {
	LogicalClock        int
	HappenedBeforeCount int
	MessageId           int
	Origin              int
}

type RecoveryRequestMessage struct {
	Source int
}

//////////////////////////////////////////////////////////////////////////////////

type OutstandingQueue []BroadcastMessage

func (q OutstandingQueue) Len() int { return len(q) }

func (q OutstandingQueue) Less(i, j int) bool {
	if q[i].LogicalClock != q[j].LogicalClock {
		return q[i].LogicalClock < q[j].LogicalClock
	}
	return q[i].HappenedBeforeCount < q[j].HappenedBeforeCount
}

func (q OutstandingQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *OutstandingQueue) Push(x interface{}) {
	broadcastMessage := x.(BroadcastMessage)
	*q = append(*q, broadcastMessage)
}

func (q *OutstandingQueue) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}

//////////////////////////////////////////////////////////////////////////////////

type ExampleBroadcaster struct {
	transport             lib.UnderlyingTransport
	deliver               lib.Deliver
	outstandingQueue      OutstandingQueue
	lastMessagePerNode    []*BroadcastMessage
	logicalClock          int
	deliveredMessageCount int
	nodeCount             int
	nodeId                int
}

func (broadcaster *ExampleBroadcaster) Broadcast(messageId int) {
	broadcaster.logicalClock++
	message := BroadcastMessage{
		broadcaster.logicalClock,
		broadcaster.deliveredMessageCount,
		messageId,
		broadcaster.nodeId}
	for i := 0; i < broadcaster.nodeCount; i++ {
		broadcaster.transport.SendMessage(message, i)
	}
}

func (broadcaster *ExampleBroadcaster) OnMessage(message interface{}, source int) {
	switch msg := message.(type) {
	case RecoveryRequestMessage:
		if broadcaster.lastMessagePerNode[msg.Source] != nil {
			broadcaster.transport.SendMessage(*broadcaster.lastMessagePerNode[msg.Source], source)
		}

	case BroadcastMessage:
		if broadcaster.lastMessagePerNode[msg.Origin] == nil ||
			msg.LogicalClock > broadcaster.lastMessagePerNode[msg.Origin].LogicalClock {
			var messageToSave BroadcastMessage = msg
			broadcaster.lastMessagePerNode[msg.Origin] = &messageToSave
			heap.Push(&broadcaster.outstandingQueue, msg)
		}

		for len(broadcaster.outstandingQueue) > 0 {
			queued := &broadcaster.outstandingQueue[0]
			if queued.HappenedBeforeCount <= broadcaster.deliveredMessageCount {
				if queued.LogicalClock > broadcaster.logicalClock {
					broadcaster.logicalClock = queued.LogicalClock
				}

				broadcaster.deliveredMessageCount++
				broadcaster.deliver(queued.MessageId)

				heap.Pop(&broadcaster.outstandingQueue)
			} else {
				break
			}
		}

	default:
		fmt.Printf("%v\n", message)
		panic("Unknown message type")
	}
}

func (broadcaster *ExampleBroadcaster) OnNodeDeath(source int) {
	message := RecoveryRequestMessage{source}
	for i := 0; i < broadcaster.nodeCount; i++ {
		if i != broadcaster.nodeId {
			broadcaster.transport.SendMessage(message, i)
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////

func NewExample(nodeId, nodeCount int, deliver lib.Deliver, transport lib.UnderlyingTransport) lib.CausalBroadcaster {
	broadcaster := ExampleBroadcaster{
		transport:             transport,
		deliver:               deliver,
		outstandingQueue:      []BroadcastMessage{},
		lastMessagePerNode:    make([]*BroadcastMessage, nodeCount),
		logicalClock:          0,
		deliveredMessageCount: 0,
		nodeCount:             nodeCount,
		nodeId:                nodeId}

	heap.Init(&broadcaster.outstandingQueue)
	return &broadcaster
}

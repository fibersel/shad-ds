// +build private

package causal_broadcast

import (
	algo "gitlab.com/slon/shad-ds/causal_broadcast/algo"
	"gitlab.com/slon/shad-ds/causal_broadcast/lib"
	"gitlab.com/slon/shad-ds/causal_broadcast/tests"
	algo2 "gitlab.com/slon/shad-ds/private/causal_broadcast/algo"
	_ "gitlab.com/slon/shad-ds/private/causal_broadcast/tests"
	"testing"

	ac130uspectre "gitlab.com/slon/shad-ds/private/causal_broadcast/ac130uspectre"
	acepeak3 "gitlab.com/slon/shad-ds/private/causal_broadcast/acepeak3"
	acherepkov "gitlab.com/slon/shad-ds/private/causal_broadcast/acherepkov"
	alexandershashkin "gitlab.com/slon/shad-ds/private/causal_broadcast/alexandershashkin"
	almaz "gitlab.com/slon/shad-ds/private/causal_broadcast/almaz"
	andrewsg "gitlab.com/slon/shad-ds/private/causal_broadcast/andrewsg"
	artemkaa "gitlab.com/slon/shad-ds/private/causal_broadcast/artemkaa"
	artli "gitlab.com/slon/shad-ds/private/causal_broadcast/artli"
	asntr "gitlab.com/slon/shad-ds/private/causal_broadcast/asntr"
	avinyukhin "gitlab.com/slon/shad-ds/private/causal_broadcast/avinyukhin"
	azik "gitlab.com/slon/shad-ds/private/causal_broadcast/azik"
	balantay_ "gitlab.com/slon/shad-ds/private/causal_broadcast/balantay_"
	bixind "gitlab.com/slon/shad-ds/private/causal_broadcast/bixind"
	carakan "gitlab.com/slon/shad-ds/private/causal_broadcast/carakan"
	chiselko6 "gitlab.com/slon/shad-ds/private/causal_broadcast/chiselko6"
	chiyar "gitlab.com/slon/shad-ds/private/causal_broadcast/chiyar"
	danlark "gitlab.com/slon/shad-ds/private/causal_broadcast/danlark"
	dimak24 "gitlab.com/slon/shad-ds/private/causal_broadcast/dimak24"
	diralik "gitlab.com/slon/shad-ds/private/causal_broadcast/diralik"
	eugney_melnikov "gitlab.com/slon/shad-ds/private/causal_broadcast/eugney_melnikov"
	fakefeik "gitlab.com/slon/shad-ds/private/causal_broadcast/fakefeik"
	galtsev "gitlab.com/slon/shad-ds/private/causal_broadcast/galtsev"
	gavr97 "gitlab.com/slon/shad-ds/private/causal_broadcast/gavr97"
	gezort "gitlab.com/slon/shad-ds/private/causal_broadcast/gezort"
	gigadesu "gitlab.com/slon/shad-ds/private/causal_broadcast/gigadesu"
	glebone "gitlab.com/slon/shad-ds/private/causal_broadcast/glebone"
	gostkin "gitlab.com/slon/shad-ds/private/causal_broadcast/gostkin"
	howl "gitlab.com/slon/shad-ds/private/causal_broadcast/howl"
	ikibardin "gitlab.com/slon/shad-ds/private/causal_broadcast/ikibardin"
	ilyasm "gitlab.com/slon/shad-ds/private/causal_broadcast/ilyasm"
	ivannechepurenco "gitlab.com/slon/shad-ds/private/causal_broadcast/ivannechepurenco"
	jakovenko_dm "gitlab.com/slon/shad-ds/private/causal_broadcast/jakovenko_dm"
	jive_jegg "gitlab.com/slon/shad-ds/private/causal_broadcast/jive_jegg"
	kaeuan "gitlab.com/slon/shad-ds/private/causal_broadcast/kaeuan"
	kborozdin "gitlab.com/slon/shad-ds/private/causal_broadcast/kborozdin"
	kkabulov "gitlab.com/slon/shad-ds/private/causal_broadcast/kkabulov"
	kombanoid "gitlab.com/slon/shad-ds/private/causal_broadcast/kombanoid"
	komissarova_polina_a "gitlab.com/slon/shad-ds/private/causal_broadcast/komissarova_polina_a"
	konarkcher "gitlab.com/slon/shad-ds/private/causal_broadcast/konarkcher"
	kreo "gitlab.com/slon/shad-ds/private/causal_broadcast/kreo"
	leshiy1295 "gitlab.com/slon/shad-ds/private/causal_broadcast/leshiy1295"
	lordprotoss "gitlab.com/slon/shad-ds/private/causal_broadcast/lordprotoss"
	makkolts "gitlab.com/slon/shad-ds/private/causal_broadcast/makkolts"
	max2103 "gitlab.com/slon/shad-ds/private/causal_broadcast/max2103"
	maximtim "gitlab.com/slon/shad-ds/private/causal_broadcast/maximtim"
	mlepekhin "gitlab.com/slon/shad-ds/private/causal_broadcast/mlepekhin"
	moskalenkoviktor "gitlab.com/slon/shad-ds/private/causal_broadcast/moskalenkoviktor"
	mpivko "gitlab.com/slon/shad-ds/private/causal_broadcast/mpivko"
	mrkastep "gitlab.com/slon/shad-ds/private/causal_broadcast/mrkastep"
	nandreyn "gitlab.com/slon/shad-ds/private/causal_broadcast/nandreyn"
	nikita_uvarov "gitlab.com/slon/shad-ds/private/causal_broadcast/nikita_uvarov"
	nina "gitlab.com/slon/shad-ds/private/causal_broadcast/nina"
	nogo "gitlab.com/slon/shad-ds/private/causal_broadcast/nogo"
	notimplemented "gitlab.com/slon/shad-ds/private/causal_broadcast/notimplemented"
	ogorodnikoff2012 "gitlab.com/slon/shad-ds/private/causal_broadcast/ogorodnikoff2012"
	ppavel96 "gitlab.com/slon/shad-ds/private/causal_broadcast/ppavel96"
	psushin "gitlab.com/slon/shad-ds/private/causal_broadcast/psushin"
	savastep "gitlab.com/slon/shad-ds/private/causal_broadcast/savastep"
	sgjurano "gitlab.com/slon/shad-ds/private/causal_broadcast/sgjurano"
	shevkunov "gitlab.com/slon/shad-ds/private/causal_broadcast/shevkunov"
	sivukhin_nikita "gitlab.com/slon/shad-ds/private/causal_broadcast/sivukhin_nikita"
	skrahot "gitlab.com/slon/shad-ds/private/causal_broadcast/skrahot"
	sul "gitlab.com/slon/shad-ds/private/causal_broadcast/sul"
	svinpapicha "gitlab.com/slon/shad-ds/private/causal_broadcast/svinpapicha"
	telenkov "gitlab.com/slon/shad-ds/private/causal_broadcast/telenkov"
	thepunchy "gitlab.com/slon/shad-ds/private/causal_broadcast/thepunchy"
	tinsane "gitlab.com/slon/shad-ds/private/causal_broadcast/tinsane"
	valeriyasinevich "gitlab.com/slon/shad-ds/private/causal_broadcast/valeriyasinevich"
	vbuchnev "gitlab.com/slon/shad-ds/private/causal_broadcast/vbuchnev"
)

func TestReferenceIsPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo2.NewReference, tests.ExtraTests, false, true)
}

func TestSolutionIsPassingExtraTests__mrkastep(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mrkastep.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__vbuchnev(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, vbuchnev.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__kombanoid(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kombanoid.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ilyasm(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ilyasm.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__artemkaa(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, artemkaa.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__acherepkov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, acherepkov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__komissarova_polina_a(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, komissarova_polina_a.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__gigadesu(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gigadesu.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ikibardin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ikibardin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__nikita_uvarov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nikita_uvarov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__max2103(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, max2103.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__howl(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, howl.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__shevkunov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, shevkunov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__kaeuan(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kaeuan.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ppavel96(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ppavel96.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__andrewsg(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, andrewsg.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__galtsev(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, galtsev.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__leshiy1295(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, leshiy1295.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__telenkov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, telenkov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__dimak24(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, dimak24.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__alexandershashkin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, alexandershashkin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__artli(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, artli.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__tinsane(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tinsane.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__gostkin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gostkin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ogorodnikoff2012(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ogorodnikoff2012.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__notimplemented(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, notimplemented.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__danlark(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, danlark.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__azik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, azik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__jakovenko_dm(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, jakovenko_dm.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__nina(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nina.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__mlepekhin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mlepekhin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__savastep(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, savastep.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__gavr97(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gavr97.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__svinpapicha(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, svinpapicha.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__kborozdin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kborozdin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__moskalenkoviktor(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, moskalenkoviktor.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__konarkcher(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, konarkcher.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__bixind(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, bixind.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__balantay_(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, balantay_.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__eugney_melnikov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, eugney_melnikov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__valeriyasinevich(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, valeriyasinevich.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ivannechepurenco(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ivannechepurenco.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__maximtim(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, maximtim.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__carakan(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, carakan.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__kkabulov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kkabulov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__thepunchy(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, thepunchy.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__nogo(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nogo.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__chiselko6(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, chiselko6.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__jive_jegg(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, jive_jegg.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__mpivko(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mpivko.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__sivukhin_nikita(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sivukhin_nikita.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__lordprotoss(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, lordprotoss.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__asntr(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, asntr.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__chiyar(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, chiyar.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__ac130uspectre(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ac130uspectre.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__acepeak3(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, acepeak3.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__makkolts(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, makkolts.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__kreo(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kreo.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__almaz(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, almaz.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__skrahot(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, skrahot.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__nandreyn(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nandreyn.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__diralik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, diralik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__sul(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sul.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__psushin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, psushin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__glebone(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, glebone.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__sgjurano(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sgjurano.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__fakefeik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, fakefeik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__gezort(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gezort.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTests__avinyukhin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, avinyukhin.ExtraTests, false, false)
}

package lib

import "time"

const MaxBroadcastsPerTest = 200
const MaxEventsPerTest = 100
const MaxSegmentsPerTest = 32
const MaxNodeCount = 32

const WarmupRounds = 100
const TeardownRounds = 1000

// Average time of one-way remote message delivery in stable segment.
const AvgDeliveryTime = 1000

// Minimum value of delay used in AfterFunc.
const MinTimerDelay = 10
const StableSegmentError = 0.1

const TestTimeout = 5 * time.Second
const TestSuitTimeout = 20 * time.Second

type BroadcastRequest struct {
	// Timestamp is relative to the begin of the sub-segment broadcast time.
	Timestamp int
	MessageId int
	Source    int
}

type SentMessage struct {
	Message          interface{}
	Source           int
	Destination      int
	SendTimestamp    int
	DeliverTimestamp int

	// True, if this is delayed event, scheduled via AfterFunc method of Context.
	TimerEvent bool
}

type NodeEvent struct {
	// Timestamp is relative to the begin of the unstable sub-segment broadcast time.
	Timestamp int
	Node      int
}

type DelayFunc func(message SentMessage) int

type TestSegment struct {
	// Timestamps for stable broadcast are counted from the moment of warmup completion.
	StableBroadcasts []BroadcastRequest
	StableDelayFunc  DelayFunc

	// Timestamps for best effort broadcasts are counted from the moment of delivery of the last stable event in the TestSegment.
	UnstableBroadcasts []BroadcastRequest
	UnstableDelayFunc  DelayFunc

	// Timestamps for node events are counted from the moment start of unstable subsegment.
	DeathEvents  []NodeEvent
	ReviveEvents []NodeEvent
}

type TestDescriptor struct {
	Author    string
	TestName  string
	NodeCount int

	Segments []TestSegment
}
